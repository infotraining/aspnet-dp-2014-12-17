﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machine.Specifications;

namespace BankAccount.Tests
{

    public class When_adding_to_list
    {
        private static List<int> _list;
        private static int _item = 1;

        private Establish context = () => _list = new List<int>();

        private Because of = () => _list.Add(_item);

        private It should_increase_count_of_items = () => _list.Count.ShouldEqual(1);

        private It should_insert_item_at_the_end = () => _list[_list.Count - 1].ShouldEqual(_item);
    }


    public class with_bank_account
    {
        protected static BankAccount _account;

        protected Establish context = () => _account = new BankAccount(1);
    }

    public class When_account_is_created : with_bank_account
    {
        private It should_have_id = () => _account.Id.ShouldEqual(1);
        private It should_have_balance_equal_to_zero = () => _account.Balance.ShouldEqual(0.0m);
        private It should_be_in_normal_state = () => _account.State.ShouldEqual(AccountState.Normal);        
    }

    class When_making_deposit_in_normal_state : with_bank_account
    {
        private Because of = () => _account.Deposit(100M);

        private It should_increase_balance_with_proper_amount = () => _account.Balance.ShouldEqual(100.0M);
        private It should_not_change_state = () => _account.State.ShouldEqual(AccountState.Normal);
    }

    public enum AccountState
    {
        Normal = 1
    }

    public class BankAccount
    {
        public int Id { get; private set; }
        public decimal Balance { get; private set; }
        public AccountState State { get; private set; }

        public BankAccount(int id)
        {
            Id = id;
            State = AccountState.Normal;
        }

        public void Deposit(decimal amount)
        {
            Balance += amount;
        }
    }
}
