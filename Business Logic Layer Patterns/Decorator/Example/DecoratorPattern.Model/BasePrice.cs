﻿using System;

namespace DecoratorPattern.Model
{
    public class BasePrice : IPrice 
    {
        private Decimal _cost;

        public decimal Cost
        {
            get { return _cost; }
            set { _cost = value; }
        }     
    }
}
