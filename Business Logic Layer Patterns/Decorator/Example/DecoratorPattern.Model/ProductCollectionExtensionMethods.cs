﻿using System.Collections.Generic;

namespace DecoratorPattern.Model
{
    public static class ProductCollectionExtensionMethods
    {
        public static void ApplyCurrencyMultiplier(this IEnumerable<Product> products)
        {
            foreach (var p in products)            
                p.Price = new CurrencyPriceDecorator(p.Price, 0.78m);            
        }

        public static void ApplyTradeDiscount(this IEnumerable<Product> products)
        {
            foreach (var p in products)            
                p.Price = new TradeDiscountPriceDecorator(p.Price);            
        }
    }
}
