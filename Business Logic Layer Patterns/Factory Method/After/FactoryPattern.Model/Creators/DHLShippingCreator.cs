﻿namespace FactoryPattern.Model.Creators
{
    public class DHLShippingCreator : ICourierCreator
    {
        public IShippingCourier CreateShippingCourier()
        {
            return new DHL();
        }
    }
}