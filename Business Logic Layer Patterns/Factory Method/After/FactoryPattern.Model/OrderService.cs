﻿namespace FactoryPattern.Model
{
    public class OrderService
    {
        private IShippingCourierFactory _shippingCourierFactory;

        public OrderService(IShippingCourierFactory shippingCourierFactory)
        {
            _shippingCourierFactory = shippingCourierFactory;
        }

        public void Dispatch(Order order)
        {
            IShippingCourier shippingCourier = _shippingCourierFactory.CreateShippingCourier(order);

            order.CourierTrackingId = shippingCourier.GenerateConsignmentLabelFor(order.DispatchAddress);    
        }
    }
}
