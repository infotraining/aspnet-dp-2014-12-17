using SpecificationPattern;

namespace FactoryPattern.Model.Specifications
{
    public class TotalCostOver100 : CompositeSpecification<Order>
    {
        private const decimal _costThreshold = 100.0M;

        public override bool IsSatisfiedBy(Order candidate)
        {
            return candidate.TotalCost > _costThreshold;
        }
    }
}