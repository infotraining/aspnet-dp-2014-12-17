﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SpecificationPattern;

namespace FactoryPattern.Model.Specifications
{
    public class WeightOver5Kg : CompositeSpecification<Order>
    {
        public const decimal _threshold = 5.0M;

        public override bool IsSatisfiedBy(Order candidate)
        {
            return candidate.WeightInKG > _threshold;
        }
    }
}
