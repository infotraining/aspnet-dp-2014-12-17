using System.Diagnostics.Contracts;

namespace BankAccount.Model
{
    public class BankAccount
    {
        public int Id { get; private set; }
        public decimal Balance { get; private set; }

        private AccountState _state;

        public AccountState Status
        {
            get { return _state; }
        }

        public BankAccount(int id, decimal balance = 0.0M)
        {
            Id = id;
            Balance = balance;
            
            _state = UpdateAccountState();
        }

        public void Deposit(decimal amount)
        {
            Contract.Assert(amount > 0.0M);

            Balance += amount;

            _state = UpdateAccountState();
        }

        public void Withdraw(decimal amount)
        {
            Contract.Assert(amount > 0.0M);

            if (_state == AccountState.Overdraft)
                throw new InsufficientFunds(string.Format("Insufficient funds on account #1{0}", Id));
            else
            {
                Balance -= amount;

                _state = UpdateAccountState();
            }
        }

        public void PayInterest()
        {
            if (_state == AccountState.Overdraft)
                Balance *= 1.2M;
            else
                Balance *= 1.1M;
        }

        private AccountState UpdateAccountState()
        {
            if (Balance >= 0)
                return AccountState.Normal;
            else
                return AccountState.Overdraft;                
        }
    }
}