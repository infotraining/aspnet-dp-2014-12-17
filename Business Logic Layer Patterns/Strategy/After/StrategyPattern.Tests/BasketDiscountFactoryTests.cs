﻿using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;
using StrategyPattern.Model;

namespace StrategyPattern.Tests
{
    [TestFixture]
    public class BasketDiscountFactoryTests
    {
        [Test]
        public void BasketDiscountFactory_Should_Return_BasketDiscountMoneyOff_When_Passed_DiscountType_Of_MoneyOff()
        {
            IBasketDiscountStrategy basketDiscountStrategy = BasketDiscountFactory.GetDiscount(DiscountType.MoneyOff);

            Assert.That(basketDiscountStrategy, Is.TypeOf(typeof(BasketDiscountMoneyOff)));
        }

        [Test]
        public void BasketDiscountFactory_Should_Return_BasketDiscountPercentageOff_When_Passed_DiscountType_Of_PercentageOff()
        {
            IBasketDiscountStrategy basketDiscountStrategy = BasketDiscountFactory.GetDiscount(DiscountType.PercentageOff);

            Assert.That(basketDiscountStrategy, Is.TypeOf(typeof(BasketDiscountPercentageOff)));
        }
    }
}
