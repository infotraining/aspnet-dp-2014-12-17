﻿using NUnit.Framework;
using NUnit.Framework.SyntaxHelpers;
using StrategyPattern.Model;

namespace StrategyPattern.Tests
{
    [TestFixture]
    public class BasketTest
    {
        [Test]
        public void Basket_With_DiscountType_Of_PercentageOff_Should_Take_15pc_Off_A_Baskets_Total_If_Over_100()
        {
            int totalAfterDiscount = 85;
            Basket basket = new Basket(DiscountType.PercentageOff) {TotalCost = 100};
            Assert.AreEqual(totalAfterDiscount, basket.GetTotalCostAfterDiscount());
        }

        [Test]
        public void Basket_With_DiscountType_Of_MoneyOff_Should_Take_10_Off_A_Baskets_Total_If_Over_100()
        {
            int totalAfterDiscount = 91;
            Basket basket = new Basket(DiscountType.MoneyOff) { TotalCost = 101 };
            Assert.AreEqual(totalAfterDiscount, basket.GetTotalCostAfterDiscount());
        }

        [Test]
        public void Basket_With_NoDiscount_Should_Not_Calculate_Any_Discount()
        {
            decimal totalAfterDiscount = 125.0M;
            Basket basket = new Basket(DiscountType.NoDiscount) { TotalCost = 125.0M };
            Assert.That(basket.GetTotalCostAfterDiscount(), Is.EqualTo(totalAfterDiscount));
        }
    }
}
