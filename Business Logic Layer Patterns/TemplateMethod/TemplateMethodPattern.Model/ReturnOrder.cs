﻿namespace TemplateMethodPattern.Model
{
    public class ReturnOrder
    {
        public ReturnAction Action { get; set; }
        public string PaymentTransactionId { get; set; }
        public decimal PricePaid { get; set; }
        public decimal PostageCost { get; set; }
        public decimal AmountToRefund { get; set; }
        public long ProductId { get; set; }
        public long QtyBeingReturned { get; set; }
    }
}
