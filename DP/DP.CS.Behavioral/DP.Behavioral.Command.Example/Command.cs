﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DP.Behavioral.Command.Example;

namespace DP.Behavioral.Command.Example
{
    public interface ICommand
    {
        void Execute();
        void Undo();
        ICommand Clone();
    }

    public abstract class CommandBase : ICommand
    {
        public virtual ICommand Clone()
        {
            return (ICommand)MemberwiseClone();
        }

        public abstract void Execute();
        public abstract void Undo();
    }

    public abstract class UndoableCommand : CommandBase
    {
        private ICommandTracker _commandTracker;

        public UndoableCommand(ICommandTracker commandTracker)
        {
            _commandTracker = commandTracker;
        }

        protected ICommandTracker CommandTracker
        {
            get { return _commandTracker; }
        }
    }


    public class CopyCommand : CommandBase
    {
        private TextBox receiver;

        public CopyCommand(TextBox receiver)
        {
            this.receiver = receiver;
        }

        #region ICommand Members

        public override void Execute()
        {
            receiver.Copy();
        }

        #endregion

        public override void Undo()
        {
        }
    }

    public class PasteCommand : UndoableCommand
    {
        string before;

        private TextBox receiver;

        public PasteCommand(TextBox receiver, ICommandTracker commandTracker)
            : base(commandTracker)
        {
            this.receiver = receiver;
        }

        #region ICommand Members

        public override void Execute()
        {
            before = receiver.Text;
            CommandTracker.Push(this.Clone());
            receiver.Paste();
        }

        #endregion

        public override void Undo()
        {
            receiver.Text = before;
        }
    }

    public class ToUpperCommand : UndoableCommand
    {
        private TextBox receiver;
        private string selectedText;
        private int selectionStarts;
        private int selectionLength;

        public ToUpperCommand(TextBox receiver, ICommandTracker commandTracker)
            : base(commandTracker)
        {
            this.receiver = receiver;
        }

        #region ICommand Members

        public override void Execute()
        {
            selectedText = receiver.SelectedText;
            selectionStarts = receiver.SelectionStart;
            selectionLength = receiver.SelectionLength;
            receiver.Text = receiver.Text.Remove(selectionStarts, selectionLength);
            receiver.Text = receiver.Text.Insert(selectionStarts, selectedText.ToUpper());
        }

        #endregion

        public override void Undo()
        {
            // TODO
        }
    }

    public class RemoveCommand : UndoableCommand
    {
        private TextBox receiver;
        private string selectedText;
        private int selectionStarts;
        private int selectionLength;

        public RemoveCommand(TextBox receiver, ICommandTracker commandTracker)
            : base(commandTracker)
        {
            this.receiver = receiver;
        }

        #region ICommand Members

        public override void Execute()
        {
            selectionStarts = receiver.SelectionStart;
            selectionLength = receiver.SelectionLength;

            if (selectionLength > 0)
            {
                selectedText = receiver.SelectedText;
                CommandTracker.Push(this.Clone());
                receiver.Text = receiver.Text.Remove(selectionStarts, selectionLength);
            }
        }

        #endregion

        public override void Undo()
        {
            receiver.Text = receiver.Text.Insert(selectionStarts, selectedText);
        }
    }

    // TODO
    //public class ToLowerCommand : ICommand
    //{
    //}

    public class UndoCommand : UndoableCommand
    {
        public UndoCommand(ICommandTracker commandTracker)
            : base(commandTracker)
        {

        }

        public override void Execute()
        {
            ICommand lastCmd = CommandTracker.Pop();
            lastCmd.Undo();
        }

        public override void Undo()
        {
        }
    }
}