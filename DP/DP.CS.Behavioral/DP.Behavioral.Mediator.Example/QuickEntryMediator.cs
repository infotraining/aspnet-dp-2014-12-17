﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DP.Behavioral.Mediator.Example
{
    public class QuickEntryMediator
    {
        private TextBox itsTextBox;
        private ListBox itsList;

        public QuickEntryMediator(TextBox t, ListBox l)
        {
            itsTextBox = t;
            itsList = l;
            itsTextBox.TextChanged += new EventHandler(TextFieldChanged);
        }
        
        private void TextFieldChanged(object source, EventArgs args)
        {
            string prefix = itsTextBox.Text;

            if (prefix.Length == 0)
            {
                itsList.ClearSelected();
                return;
            }

            ListBox.ObjectCollection listItems = itsList.Items;
            bool found = false;
            for (int i = 0; found == false &&
                      i < listItems.Count; i++)
            {
                Object o = listItems[i];
                String s = o.ToString();
                if (s.StartsWith(prefix))
                {
                    itsList.SetSelected(i, true);
                    found = true;
                }
            }
            if (!found)
            {
                itsList.ClearSelected();
            }
        }
    }

}
