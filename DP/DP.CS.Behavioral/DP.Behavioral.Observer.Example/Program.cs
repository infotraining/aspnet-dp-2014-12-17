﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Behavioral.Observer.Example
{
    /// <summary>
    /// Observer Design Pattern.
    /// </summary>
    class MainApp
    {
        /// <summary>
        /// Entry point into console application.
        /// </summary>
        static void Main()
        {
            Investor sorros = new Investor("Sorros");
            // Create IBM stock and attach investors
            Stock ibm = new Stock("IBM", 120.00);
            ibm.Attach(sorros);
            ibm.Attach(new Investor("Berkshire"));

            Stock reuters = new Stock("Reuters", 100.0);

            reuters.Attach(sorros);

            reuters.Price = 130.0;
            reuters.Price = 190.0;

            // Fluctuating prices will notify investors
            ibm.Price = 120.10;
            ibm.Price = 121.00;

            ibm.Detach(sorros);

            ibm.Price = 120.50;
            ibm.Price = 120.75;
        }
    }

    // Custom event arguments
    public class ChangeEventArgs : EventArgs
    {
        private string symbol;
        private double price;

        // Constructor
        public ChangeEventArgs(string symbol, double price)
        {
            this.symbol = symbol;
            this.price = price;
        }

        // Properties
        public double Price
        {
            get { return price; }
            set { price = value; }
        }

        public string Symbol
        {
            get { return symbol; }
            set { symbol = value; }
        }
    }

    // "Subject" 
    public class Stock
    {
        protected string symbol;
        protected double price;

        // Constructor
        public Stock(string symbol, double price)
        {
            this.symbol = symbol;
            this.price = price;
        }

        // Event
        public event EventHandler<ChangeEventArgs> PriceChanged;
        
        // Invoke the Change event
        public virtual void OnPriceChanged(ChangeEventArgs e)
        {
            if (PriceChanged != null)
            {
                PriceChanged(this, e);
            }
        }

        public void Attach(Investor investor)
        {
            PriceChanged += new EventHandler<ChangeEventArgs>(investor.Update);
        }

        public void Detach(Investor investor)
        {
            PriceChanged -= new EventHandler<ChangeEventArgs>(investor.Update);
        }

        // Properties
        public double Price
        {
            get { return price; }
            set
            {
                if (price != value)
                {
                    Console.WriteLine("\nPrice of {0} has been changed...", Symbol);
                    price = value;
                    OnPriceChanged(new ChangeEventArgs(symbol, price));
                }
            }
        }

        public string Symbol
        {
            get { return symbol; }
            set { symbol = value; }
        }
    }

    // "Observer"
    interface IInvestor
    {
        void Update(object sender, ChangeEventArgs e);
    }

    // "ConcreteObserver"
    public class Investor : IInvestor
    {
        private string name;

        // Constructor
        public Investor(string name)
        {
            this.name = name;
        }

        public void Update(object sender, ChangeEventArgs e)
        {
            Console.WriteLine("Notified {0} of {1}'s " +
                "change to {2:C}", name, e.Symbol, e.Price);
        }
    }
}
