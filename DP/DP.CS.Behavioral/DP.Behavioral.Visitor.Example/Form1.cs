﻿#region Using directives

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

#endregion

namespace Tokenizer
{
    partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Open_Click(object sender, EventArgs e)
        {
            SourceFile source = new SourceFile();
            ColorSyntaxVisitor visitor = new ColorSyntaxVisitor(codeText);
            source.Accept(visitor);
        }

        private void codeText_TextChanged(object sender, EventArgs e)
        {

        }
    }
}