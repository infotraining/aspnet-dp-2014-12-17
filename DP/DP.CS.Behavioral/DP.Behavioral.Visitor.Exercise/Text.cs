﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Linq;

namespace Drawing
{
    [Serializable()]
    public class Text : LegacyCode.Paragraph, IShape
    {
        public Text(int x, int y, string content)
            :base(x, y, content)
        {
        }        

        #region IShape Members

        public void Draw()
        {
            Render();
        }

        public void Move(int dx, int dy)
        {
            PosX += dx;
            PosY += dy;
        }

        public IShape Clone()
        {
            IShape clonedShape = null;
            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();

                formatter.Serialize(stream, this);
                stream.Seek(0, SeekOrigin.Begin);
                clonedShape = formatter.Deserialize(stream) as IShape;
            }

            return clonedShape;
        }

        #endregion

        public class Factory : Shape.Factory
        {
            protected override IShape CreateShape(params object[] args)
            {
                XElement imageElement = (XElement)args[0];
                int x = int.Parse(imageElement.Element("Point").Element("X").Value);
                int y = int.Parse(imageElement.Element("Point").Element("Y").Value);
                string content = imageElement.Element("Content").Value;

                return new Text(x, y, content);
            }
        }

        #region IShape Members


        public void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }

        #endregion
    }
}
