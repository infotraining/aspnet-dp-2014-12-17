﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MVC
{
    public interface IBeatController
    {
        void On();
        void Off();
        void IncreaseBpm();
        void DecreaseBpm();
        void SetBpm(int bpm);
        void SetForm(FormController f);
    }

    public class BeatController : IBeatController
    {
        IBeatModel model;
        FormView view;
        FormController form;

        public BeatController(IBeatModel model, FormView view)
        {
            this.model = model;
            this.view = view;
        }

        #region IBeatController Members

        public void On()
        {
            model.On();
            form.OnMenuItem.Enabled = false;
            form.OffMenuItem.Enabled = true;
        }

        public void Off()
        {
            model.Off();
            form.OnMenuItem.Enabled = true;
            form.OffMenuItem.Enabled = false;
        }

        public void IncreaseBpm()
        {
            model.Bpm += 10;
        }

        public void DecreaseBpm()
        {
            model.Bpm -= 10;
        }

        public void SetBpm(int bpm)
        {
            model.Bpm = bpm;
        }

        public void SetForm(FormController f)
        {
            form = f;
        }

        #endregion
    }
}
