﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace MVC
{
    public delegate void BeatHandler();

    public interface IBeatModel
    {
        void On();
        void Off();
        int Bpm { get; set; }
        event BeatHandler Beat;
    }

    public class BeatModel : IBeatModel
    {
        private int bpm = 60;
        private bool isOn = false;
        Thread thread;

        public BeatModel()
        {
            thread = new Thread(Run);
            thread.IsBackground = true;
            thread.Start();
        }

        #region IBeatModel Members

        public void On()
        {
            isOn = true;
        }

        public void Off()
        {
            isOn = false;
        }

        public int Bpm
        {
            get
            {
                return bpm;
            }

            set
            {
                bpm = value;
            }
        }

        public event BeatHandler Beat;

        #endregion
        
        void Run()
        {
            while (true)
            {
                Thread.Sleep((int)((60.0/bpm)*1000));
                if (isOn)
                    Beat();
            }
        }
    }
}
