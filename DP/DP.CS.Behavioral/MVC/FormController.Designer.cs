﻿namespace MVC
{
    partial class FormController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtBpm = new System.Windows.Forms.TextBox();
            this.btnDecreaseBpm = new System.Windows.Forms.Button();
            this.btnIncreaseBpm = new System.Windows.Forms.Button();
            this.btnSetBpm = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.bpmToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemOn = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemOff = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Podaj BPM:";
            // 
            // txtBpm
            // 
            this.txtBpm.Location = new System.Drawing.Point(82, 34);
            this.txtBpm.Name = "txtBpm";
            this.txtBpm.Size = new System.Drawing.Size(117, 20);
            this.txtBpm.TabIndex = 1;
            this.txtBpm.TextChanged += new System.EventHandler(this.txtBpm_TextChanged);
            // 
            // btnDecreaseBpm
            // 
            this.btnDecreaseBpm.Location = new System.Drawing.Point(16, 73);
            this.btnDecreaseBpm.Name = "btnDecreaseBpm";
            this.btnDecreaseBpm.Size = new System.Drawing.Size(120, 23);
            this.btnDecreaseBpm.TabIndex = 2;
            this.btnDecreaseBpm.Text = "<<";
            this.btnDecreaseBpm.UseVisualStyleBackColor = true;
            this.btnDecreaseBpm.Click += new System.EventHandler(this.btnDecreaseBpm_Click);
            // 
            // btnIncreaseBpm
            // 
            this.btnIncreaseBpm.Location = new System.Drawing.Point(160, 73);
            this.btnIncreaseBpm.Name = "btnIncreaseBpm";
            this.btnIncreaseBpm.Size = new System.Drawing.Size(120, 23);
            this.btnIncreaseBpm.TabIndex = 3;
            this.btnIncreaseBpm.Text = ">>";
            this.btnIncreaseBpm.UseVisualStyleBackColor = true;
            this.btnIncreaseBpm.Click += new System.EventHandler(this.btnIncreaseBpm_Click);
            // 
            // btnSetBpm
            // 
            this.btnSetBpm.Location = new System.Drawing.Point(205, 32);
            this.btnSetBpm.Name = "btnSetBpm";
            this.btnSetBpm.Size = new System.Drawing.Size(75, 23);
            this.btnSetBpm.TabIndex = 4;
            this.btnSetBpm.Text = "Set Bpm";
            this.btnSetBpm.UseVisualStyleBackColor = true;
            this.btnSetBpm.Click += new System.EventHandler(this.btnSetBpm_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bpmToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(292, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // bpmToolStripMenuItem
            // 
            this.bpmToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemOn,
            this.menuItemOff});
            this.bpmToolStripMenuItem.Name = "bpmToolStripMenuItem";
            this.bpmToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.bpmToolStripMenuItem.Text = "&Bpm";
            // 
            // menuItemOn
            // 
            this.menuItemOn.Name = "menuItemOn";
            this.menuItemOn.Size = new System.Drawing.Size(152, 22);
            this.menuItemOn.Text = "&On";
            this.menuItemOn.Click += new System.EventHandler(this.menuItemOn_Click);
            // 
            // menuItemOff
            // 
            this.menuItemOff.Enabled = false;
            this.menuItemOff.Name = "menuItemOff";
            this.menuItemOff.Size = new System.Drawing.Size(152, 22);
            this.menuItemOff.Text = "O&ff";
            this.menuItemOff.Click += new System.EventHandler(this.menuItemOff_Click);
            // 
            // FormController
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 108);
            this.Controls.Add(this.btnSetBpm);
            this.Controls.Add(this.btnIncreaseBpm);
            this.Controls.Add(this.btnDecreaseBpm);
            this.Controls.Add(this.txtBpm);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormController";
            this.Text = "FormController";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBpm;
        private System.Windows.Forms.Button btnDecreaseBpm;
        private System.Windows.Forms.Button btnIncreaseBpm;
        private System.Windows.Forms.Button btnSetBpm;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem bpmToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuItemOn;
        private System.Windows.Forms.ToolStripMenuItem menuItemOff;
    }
}