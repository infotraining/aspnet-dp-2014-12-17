﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MVC
{
    public partial class FormController : Form
    {
        private IBeatController controller;

        public ToolStripMenuItem OnMenuItem { get { return menuItemOn; } }
        public ToolStripMenuItem OffMenuItem { get { return menuItemOff; } }
        public TextBox TxtBpmValue { get { return txtBpm; } }

        public FormController(IBeatController c)
        {
            controller = c;
            controller.SetForm(this);
            InitializeComponent();
        }

        private void txtBpm_TextChanged(object sender, EventArgs e)
        {
        }

        private void btnDecreaseBpm_Click(object sender, EventArgs e)
        {
            controller.DecreaseBpm();
        }

        private void btnIncreaseBpm_Click(object sender, EventArgs e)
        {
            controller.IncreaseBpm();
        }

        private void btnSetBpm_Click(object sender, EventArgs e)
        {
            int bpmValue = int.Parse(txtBpm.Text);
            controller.SetBpm(bpmValue);
        }

        private void menuItemOn_Click(object sender, EventArgs e)
        {
            controller.On();
        }

        private void menuItemOff_Click(object sender, EventArgs e)
        {
            controller.Off();
        }
    }
}
