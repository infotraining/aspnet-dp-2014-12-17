﻿namespace MVC
{
    partial class FormView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.beatBar = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // beatBar
            // 
            this.beatBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.beatBar.Location = new System.Drawing.Point(13, 6);
            this.beatBar.Name = "beatBar";
            this.beatBar.Size = new System.Drawing.Size(267, 43);
            this.beatBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.beatBar.TabIndex = 0;
            // 
            // FormView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 61);
            this.Controls.Add(this.beatBar);
            this.Name = "FormView";
            this.Text = "View";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ProgressBar beatBar;
    }
}

