﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace MVC
{
    public partial class FormView : Form
    {
        private void ChangeBarValue(int val)
        {
            if (!beatBar.InvokeRequired)
                beatBar.Value = 80;
            else
            {
                this.Invoke(new MethodInvoker(delegate() { beatBar.Value = val; }));
            }
        }

        public void OnBeat()
        {
            ChangeBarValue(80);

            for (int i = 80; i >= 0; i-=10)
            {
                Thread.Sleep(20);
                ChangeBarValue(i);
            }
        }

        public FormView()
        {
            InitializeComponent();
        }
    }
}
