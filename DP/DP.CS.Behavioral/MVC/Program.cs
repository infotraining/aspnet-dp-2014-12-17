﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace MVC
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            FormView view = new FormView();
            IBeatModel model = new BeatModel();
            IBeatController controller = new BeatController(model, view);
            FormController form = new FormController(controller);
            model.Beat += new BeatHandler(view.OnBeat);
            view.Show();
            Application.Run(form);
        }
    }
}
