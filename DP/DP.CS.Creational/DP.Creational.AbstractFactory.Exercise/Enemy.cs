﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Creational.AbstractFactory.Exercise
{
    public abstract class Enemy
    {
        public abstract void Fight();
    }

    public abstract class Soldier : Enemy
    {  
    }

    public abstract class Monster : Enemy
    {
    }

    public abstract class SuperMonster : Enemy
    {
    }

    // Enemies - Level 1
    public class SillySoldier : Soldier
    {
        public override void Fight()
        {
            Console.WriteLine("SillySoldier.Fight()");
        }
    }

    public class SillyMonster : Monster
    {
        public override void Fight()
        {
            Console.WriteLine("SillyMonster.Fight()");
        }
    }

    public class SillySuperMonster : SuperMonster
    {
        public override void Fight()
        {
            Console.WriteLine("SillySuperMonster.Fight()");
        }
    }

    // Enemies - Level 2
    public class BadSoldier : Soldier
    {
        public override void Fight()
        {
            Console.WriteLine("BadSoldier.Fight()");
        }
    }

    public class BadMonster : Monster
    {
        public override void Fight()
        {
            Console.WriteLine("BadMonster.Fight()");
        }
    }

    public class BadSuperMonster : SuperMonster
    {
        public override void Fight()
        {
            Console.WriteLine("BadSuperMonster.Fight()");
        }
    }

    // Enemies - Level 3
    public class BloodySoldier // TODO
    {
    }

    public class BloodyMonster // TODO
    {
    }

    public class BloodySuperMonster // TODO
    {
    }
}
