﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;

namespace DP.Creational.Builder.Example.Survey
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            WinFormsSurveyBuilder builder = new WinFormsSurveyBuilder();

            SurveyXmlParser director = new SurveyXmlParser(builder);
            director.BuildSurvey("survey.xml");

            panel1.Controls.Add(builder.GetSurvey());
        }
    }
}
