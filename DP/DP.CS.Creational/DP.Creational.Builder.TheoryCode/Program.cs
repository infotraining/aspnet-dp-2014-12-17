﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Creational.Builder.TheoryCode
{
    // "Director"
    class Director
    {
        public void Construct(IBuilder builder)
        {
            builder.BuildPartA();
            builder.BuildPartB();
            builder.BuildPartB();
        }
    }

    // "IBuilder"
    interface IBuilder
    {
        void BuildPartA();
        void BuildPartB();
        Product GetResult();
    }

    // "ConcreteBuilder"
    class ConcreteBuilder1 : IBuilder
    {
        private Product product = new Product();

        #region IBuilder Members

        public void BuildPartA()
        {
            product.Add("PartA");
        }

        public void BuildPartB()
        {
            product.Add("PartB");
        }

        public Product GetResult()
        {
            return product;
        }

        #endregion
    }

    // "ConcreteBuilder"
    class ConcreteBuilder2 : IBuilder
    {
        private Product product = new Product();

        #region IBuilder Members

        public void BuildPartA()
        {
            product.Add("PartX");
        }

        public void BuildPartB()
        {
            product.Add("PartY");
        }

        public Product GetResult()
        {
            return product;
        }

        #endregion
    }

    // "Product"
    class Product
    {
        List<string> parts = new List<string>();

        public void Add(string part)
        {
            parts.Add(part);
        }

        public void Display()
        {
            Console.WriteLine("\nProduct parts -----------");
            foreach (string part in parts)
                Console.Write(part + " ");
            Console.WriteLine();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Director director = new Director();

            IBuilder b1 = new ConcreteBuilder1();
            IBuilder b2 = new ConcreteBuilder2();

            director.Construct(b1);
            Product p1 = b1.GetResult();
            p1.Display();

            director.Construct(b2);
            Product p2 = b2.GetResult();
            p2.Display();

            Console.ReadKey();
        }
    }
}
