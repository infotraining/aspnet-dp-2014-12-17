﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Drawing;

namespace DP.Creational.FactoryMethod.Example
{
    public class Square : Shape
    {
        int Size { get; set; }

        public Square(int x, int y, int size) 
        {
            AddPoint(new Point(x, y));
            Size = size;
        }

        public override void Draw()
        {
            Console.WriteLine(string.Format("Drawing square at {0} with size {1}", GetPoint(0), Size));
        }

        public class Factory : ShapeFactory
        {
            protected override IShape CreateShape(params object[] args)
            {
                XElement circleElement = (XElement)args[0];
                int x = int.Parse(circleElement.Element("Point").Element("X").Value);
                int y = int.Parse(circleElement.Element("Point").Element("Y").Value);
                int size = int.Parse(circleElement.Element("Size").Value);

                return new Square(x, y, size);

            }
        }
    }
}
