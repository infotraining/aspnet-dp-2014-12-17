﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Creational.FactoryMethod.Exercise2
{
    class Program
    {
        static HRInfo GetHRInfo(Employee emp)
        {
            if ((emp is Salaried) || (emp is Hourly))
                return new StdInfo(emp);
            else if (emp is Temp)
                return new TempInfo(emp);


            return null;
        }

        static void Main(string[] args)
        {
            Employee[] employees = { new Salaried(1, "Kowalski Jan"), new Hourly(2, "Nowak Adam"), 
                new Salaried(3, "Iksińska Ewa"), new Temp(4, "Miś Ksawery"), 
                new Hourly(5, "Smith John"), new Temp(6, "Nijak Tadeusz"), new Temp(7, "Jakiś Tadeusz") { IsActive = true } };

            foreach (Employee e in employees)
            {
                HRInfo hrinfo = GetHRInfo(e);
                hrinfo.Info();
            }

            Console.ReadKey();
        }
    }
}
