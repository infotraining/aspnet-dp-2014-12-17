﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Drawing
{
    public class Circle : Shape
    {
        public int Radius { get; set; }

        public Circle(int x, int y, int radius)
        {
            AddPoint(new Point(x, y));
            Radius = radius;
        }

        public override void Draw()
        {
            Console.WriteLine("Drawing a circle at {0} with radius {1}", GetPoint(0), Radius);
        }
    }
}
