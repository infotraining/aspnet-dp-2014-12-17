﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Creational.FactoryMethod.Exercise
{
    // TODO: Zaimplementowac w klasie Shape i w klasach pochodnych wzorzec Prototype

    // "Shape"
    [Serializable()]
    public abstract class Shape
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Shape()
        {
        }

        public Shape(int x, int y)
        {
            X = x;
            Y = y;
        }

        public abstract void Draw();

        public virtual Shape Clone()
        {
            return this.MemberwiseClone() as Shape;
        }

        public static class CloneFactory
        {
            static Dictionary<string, Shape> creators = new Dictionary<string, Shape>();

            public static Shape CreateShape(string shapeId)
            {
                return creators[shapeId].Clone();
            }

            public static void Register(string id, Shape s)
            {
                creators.Add(id, s);
            }

            public static void Unregister(string id)
            {
                creators.Remove(id);
            }
        }
    }
}
