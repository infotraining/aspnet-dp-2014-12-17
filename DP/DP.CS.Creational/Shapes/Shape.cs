﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shapes
{
    // "Shape"
    public abstract class Shape
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Shape()
        {
        }

        public Shape(int x, int y)
        {
            X = x;
            Y = y;
        }

        public abstract void Draw();

        public abstract class Factory
        {
            static Dictionary<string, Factory> creators = new Dictionary<string, Factory>();

            public static Shape Create(string shapeId)
            {
                return creators[shapeId].CreateShape();
            }

            protected abstract Shape CreateShape(); // factory method

            public static void Register(string id, Factory c)
            {
                creators.Add(id, c);
            }

            public static void Unregister(string id)
            {
                creators.Remove(id);
            }
        }
    }
}
