﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Drawing;
using System.IO;
using System.Xml.Linq;

namespace ShapesRevisited
{
    public class Document
    {
        static Document()
        {
            Shape.Factory.Register("Circle", new Circle.Factory());
            Shape.Factory.Register("Line", new Line.Factory());
            Shape.Factory.Register("Rectangle", new Rectangle.Factory());
            Shape.Factory.Register("Image", new Image.Factory());
        }

        List<IShape> shapes = new List<IShape>();
         
        public virtual void Load(string path)
        {
            XElement root = XElement.Load(path);

            foreach (XElement element in root.Elements("Shape"))
            {
                string id = element.Attribute("Id").Value;
                try
                {
                    IShape s = Shape.Factory.Create(id, element);
                    shapes.Add(s);
                }
                catch (KeyNotFoundException e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        public void Draw()
        {
            foreach (IShape s in shapes)
                s.Draw();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Document doc = new Document();
            doc.Load("../../graphics.xml");
            doc.Draw();
        }
    }
}
