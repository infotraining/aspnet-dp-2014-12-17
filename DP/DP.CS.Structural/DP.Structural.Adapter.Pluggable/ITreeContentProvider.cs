﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Structural.Adapter.Pluggable
{
    public interface ITreeContentProvider<T>
    {
        IEnumerable<T> GetChildren(T node);
        T GetParent(T node);
        bool HasChildren(T node);
        string GetText(T node);
    }
}
