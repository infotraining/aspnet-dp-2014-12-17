﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Structural.Adapter.TheoryCode
{
    /// <summary>
    /// Adapter Design Pattern.
    /// </summary>
    class MainApp
    {
        /// <summary>
        /// Entry point into console application.
        /// </summary>
        static void Main()
        {
            // using ObjectAdapter
            Console.WriteLine("ObjectAdapter:");
            ITarget target = new ObjectAdapter(new Adaptee());
            target.Request();

            // using ClassAdapter
            Console.WriteLine("ClassAdapter:");
            target = new ClassAdapter();
            target.Request();
        }
    }

    // "Target" 
    interface ITarget
    {
        void Request();
    }

    // "Adaptee"
    class Adaptee
    {
        public void SpecificRequest()
        {
            Console.WriteLine("Called SpecificRequest()");
        }
    }

    // "Adapter" 
    class ObjectAdapter : ITarget
    {
        private Adaptee adaptee;

        public ObjectAdapter(Adaptee a)
        {
            adaptee = a;
        }

        public void Request()
        {
            // additional work possible
            adaptee.SpecificRequest();
        }
    }

    // "Adapter"
    class ClassAdapter : Adaptee, ITarget
    {
        #region ITarget Members

        public void Request()
        {
            this.SpecificRequest();
        }

        #endregion
    }
}
