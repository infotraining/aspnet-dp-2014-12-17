﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Drawing;
using System.Xml.Linq;
using System.Drawing;

namespace DP.Structural.Bridge.Example
{
    public class Document
    {
        List<IShape> shapes = new List<IShape>();
        IShapeImpl impl;

        public Document(IShapeImpl impl)
        {
            this.impl = impl;
        }

        public virtual void Load(string path)
        {
            XElement root = XElement.Load(path);

            foreach (XElement element in root.Elements("Shape"))
            {
                string id = element.Attribute("Id").Value;
                try
                {
                    IShape s = ShapeFactory.Create(id, element);
                    s.Implementation = impl;
                    shapes.Add(s);
                }
                catch (KeyNotFoundException e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        public void Show()
        {
            foreach (IShape s in shapes)
                s.Draw();
        }
    }
}
