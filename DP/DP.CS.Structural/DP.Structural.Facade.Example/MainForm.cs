﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DP.Structural.Facade.Example
{
    public partial class MainForm : Form
    {
        DBase database;

        public MainForm()
        {
            InitializeComponent();

            database = new AccessDbase("C:\\Training\\Northwind.mdb");
            //database = new SqlServerDBase("Northwind");
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void btnExecute_Click(object sender, EventArgs e)
        {

            try
            {
                if (rbtTable.Checked) // otwarcie tabeli
                {
                    dgvResults.DataSource = database.openTable(txtTableSQL.Text);
                }
                else
                {
                    dgvResults.DataSource = database.openQuery(txtTableSQL.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }
    }
}
