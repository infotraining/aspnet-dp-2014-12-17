﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace DP.Structural.Facade.TableDataGateway
{
    class Client
    {
        ShipperGateway shipperGateway;

        public Client(ShipperGateway shipperGateway)
        {
            this.shipperGateway = shipperGateway;
        }

        public void TestGateway()
        {
            Shipper s1 = shipperGateway.Find(1);
            Console.WriteLine("{0} {1} {2}", s1.Id, s1.CompanyName, s1.Phone);

            Shipper s2 = new Shipper { Id = 4, CompanyName = "TestShipper", Phone = "+48 12 444-00-11" };
            shipperGateway.Insert(s2);

            Shipper s3 = shipperGateway.Find(4);
            Console.WriteLine("{0} {1} {2}", s3.Id, s3.CompanyName, s3.Phone);
        }
        
    }

    class Program
    {
        static void Main(string[] args)
        {
            SqlConnectionStringBuilder connStrBld = new SqlConnectionStringBuilder();
            connStrBld.DataSource = "localhost";
            connStrBld.InitialCatalog = "Northwind";
            connStrBld.IntegratedSecurity = true;

            SqlConnection connection = new SqlConnection(connStrBld.ConnectionString);
            connection.Open();

            ShipperGateway shipperGateway = new DbShipperGateway(connection);
            Client client = new Client(shipperGateway);
            client.TestGateway();
        }
    }
}
