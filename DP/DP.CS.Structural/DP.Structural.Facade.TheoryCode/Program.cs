﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library;

namespace DP.Structural.Facade_TheoryCode
{
    class Program
    {
        static void Main(string[] args)
        {
            Facade.Operation1();
            Facade.Operation2();

            Console.ReadKey();
        }
    }
}
