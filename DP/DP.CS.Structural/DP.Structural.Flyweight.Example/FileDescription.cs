﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;

namespace DP.Structural.Flyweight.Example
{
    interface IFileIcon
    {
        void Draw(Graphics gc, int x, int y);
    }

    class FileIcon : IFileIcon
    {
        Image thumbnail;

        #region IFileIcon Members

        public FileIcon(string fileName)
        {
            string path = "FileIcons\\" + fileName;
            thumbnail = new Bitmap(path);
        }

        public void Draw(Graphics gc, int x, int y)
        {
            gc.DrawImage(thumbnail, x, y);
        }

        #endregion
    }

    // klasa singletonowa
    class FileIconRepository
    {
        Dictionary<string, IFileIcon> flyweights = new Dictionary<string, IFileIcon>();
        public IFileIcon DefaultFileIcon { get; set; }

        public static readonly FileIconRepository instance = new FileIconRepository();

        public static FileIconRepository Instance { get { return instance; } }

        static FileIconRepository() {}

        private FileIconRepository()
        {
            DirectoryInfo dirInfo = new DirectoryInfo("FileIcons");
            foreach (FileInfo fi in dirInfo.GetFiles())
            {
                if (fi.Name == "default.png")
                    DefaultFileIcon = new FileIcon("default.png");
                else
                {
                    string key = fi.Name.Substring(0, fi.Name.Length - 4);
                    flyweights["."+key] = new FileIcon(fi.Name);
                }
            }
        }

        public IFileIcon this[string index]
        {
            get 
            {
                if (!flyweights.ContainsKey(index))
                    return DefaultFileIcon;
                return flyweights[index];
            }

            set
            {
                flyweights[index] = value;
            }
        }
    }

    class FileDescription
    {
        IFileIcon ficon;

        public string Name { get; private set; }
        public string ShortName { get; private set; }
        public string Extension { get; private set; }

        public FileDescription(string name, string extension)
        {
            ficon = FileIconRepository.Instance[extension];
            Name = name;
            ShortName = GetShortName(name);
            Extension = extension;
        }

        public void Draw(Graphics gc, int x, int y)
        {
            ficon.Draw(gc, x, y);
            Font font = new Font("Verdana", 8);
            StringFormat strFrmt = new StringFormat();
            strFrmt.Alignment = StringAlignment.Center;
            strFrmt.LineAlignment = StringAlignment.Center;
            strFrmt.Trimming = StringTrimming.EllipsisPath;
            Rectangle r = new Rectangle(x - 10, y + 82, 92, 30);
            gc.DrawString(ShortName, font, Brushes.Black, r, strFrmt);
        }

        private string GetShortName(string name)
        {
            if (name.Length <= 15)
                return name;
            int start = 5;
            int length = name.Length;
            int extensionLength = name.Length - name.LastIndexOf('.');
            string result = name.Replace(name.Substring(start, length - (start + extensionLength + 1)), "...");

            return result;
        }
    }
}
