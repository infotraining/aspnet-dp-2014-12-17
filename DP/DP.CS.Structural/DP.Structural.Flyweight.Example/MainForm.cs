﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace DP.Structural.Flyweight.Example
{
    public partial class MainForm : Form
    {
        List<FileDescription> files = new List<FileDescription>();

        public MainForm()
        {
            InitializeComponent();
            this.Paint += new PaintEventHandler(MainForm_Paint);
        }

        void MainForm_Paint(object sender, PaintEventArgs e)
        {
            pnlCanvas.Refresh();
        }

        private void btnFindDir_Click(object sender, EventArgs e)
        {
            if (folderBrowserDlg.ShowDialog() == DialogResult.OK)
            {
                files.Clear();
                string path = folderBrowserDlg.SelectedPath;
                txtDir.Text = path;
                DirectoryInfo dirInfo = new DirectoryInfo(path);
                foreach(FileInfo fi in dirInfo.GetFiles())
                {
                    files.Add(new FileDescription(fi.Name, fi.Extension));
                }
            }

            this.Refresh();
        }

        private void pnlCanvas_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.TranslateTransform(pnlCanvas.AutoScrollPosition.X, pnlCanvas.AutoScrollPosition.Y);

            int startX = 50;
            int startY = 50;

            foreach (FileDescription fd in files)
            {
                fd.Draw(e.Graphics, startX, startY);
                startX += 90;

                if (startX + 90 > pnlCanvas.Width)
                {
                    startX = 50;
                    startY += 130;
                }
            }

            pnlCanvas.AutoScrollMinSize = new Size(0, startY);
        }

        private void pnlCanvas_Scroll(object sender, ScrollEventArgs e)
        {
            Invalidate();
        }
    }
}
