﻿namespace QueryObject.Infrastructure.Query
{
    public enum CriteriaOperator
    {
        Equal,
        LessThanOrEqual,
        NotApplicable
        // TODO: Implement remainder of the criteria operators...
    }
}
