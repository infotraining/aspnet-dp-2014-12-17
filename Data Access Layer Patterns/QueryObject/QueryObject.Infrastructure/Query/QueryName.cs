﻿namespace QueryObject.Infrastructure.Query
{
    public enum QueryName
    {       
        Dynamic = 0,
        RetrieveOrdersUsingAComplexQuery = 1
    }
}
