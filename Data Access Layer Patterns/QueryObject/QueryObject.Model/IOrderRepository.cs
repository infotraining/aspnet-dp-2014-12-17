﻿using System.Collections.Generic;
using QueryObject.Infrastructure.Query;

namespace QueryObject.Model
{
    public interface IOrderRepository
    {       
         IEnumerable<Order> FindBy(Query query);
         IEnumerable<Order> FindBy(Query query, int index, int count);         
    }
}
