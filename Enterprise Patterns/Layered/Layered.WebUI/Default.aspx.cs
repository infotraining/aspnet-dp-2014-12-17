﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Layered.Model;
using Layered.Presentation;
using StructureMap;

namespace Layered.WebUI
{
    public partial class Default : System.Web.UI.Page, IProductListView
    {
        private ProductListPresenter _presenter;

        protected void Page_Init(object sender, EventArgs e)
        {
            _presenter = new ProductListPresenter(this, ObjectFactory.GetInstance<Service.ProductService>());

            ddlCustomerType.SelectedIndexChanged += (o, args) => _presenter.Display();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _presenter.Display();
            }
        }

        public void Diplay(IList<Service.ProductViewModel> products)
        {
            rptProducts.DataSource = products;
            rptProducts.DataBind();
        }

        public Model.CustomerType CustomerType
        {
            get { return (CustomerType) Enum.ToObject(typeof (CustomerType), int.Parse(this.ddlCustomerType.SelectedValue)); }
        }

        public string ErrorMessage
        {
            set { lblErrorMessage.Text = string.Format("<p><strong>Error</strong></br>{0}</p>", value); }
        }
    }
}