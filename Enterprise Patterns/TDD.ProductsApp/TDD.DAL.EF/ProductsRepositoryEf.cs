﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using TDD.Model;

namespace TDD.DAL.EF
{
    public class ProductsRepositoryEf : IProductRepository
    {
        private readonly ProductsAppContext _context;

        public ProductsRepositoryEf(ProductsAppContext context)
        {
            _context = context;
        }

        public IEnumerable<Product> GetAll()
        {
            return _context.Products.ToList();
        }

        public Product FindByName(string name)
        {
            return _context.Products.FirstOrDefault(p => p.Name == name);
        }

        public void Add(Product product)
        {
            _context.Entry(product).State = EntityState.Added;
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }
    }

}
