﻿@HomePageUI
Feature: BrowsingProducts
	In order to find product
	As a user
	I want to browse products

Scenario: Page title
	When I navigate to Home page
	Then the title of the page Wishlist is displayed

Scenario: List of products
	When I navigate to Home page
	Then the list of products in alphabetical order is displayed
		| Id | ProductName           |
		| 1  | DevExpress            |
		| 2  | Resharper             |
		| 3  | VS Ulitimator Premium |
