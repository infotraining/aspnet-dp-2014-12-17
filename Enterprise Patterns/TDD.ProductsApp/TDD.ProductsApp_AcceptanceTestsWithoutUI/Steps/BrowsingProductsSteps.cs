﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Machine.Specifications;
using TDD.DAL.EF;
using TDD.Model;
using TDD.WebUi.Mvc;
using TDD.WebUi.Mvc.Controllers;
using TechTalk.SpecFlow;
using NUnit.Framework;
using Microsoft.Practices.Unity;
using TechTalk.SpecFlow.Assist;
using FluentAssertions;

namespace TDD.ProductsApp_AcceptanceTestsWithoutUI.Steps
{
    public class DbSetup : DropCreateDatabaseAlways<ProductsAppContext>
    {
        protected override void Seed(ProductsAppContext context)
        {
            context.Products.Add(new Product() {Id = 1, Name = "DevExpress"});
            context.Products.Add(new Product() {Id = 2, Name = "Resharper"});
            context.Products.Add(new Product() {Id = 3, Name = "VS Ulitimator Premium"});
        }
    }

    [Binding]
    public class WithEveryScenario
    {
        [BeforeScenario]
        public void BeforeEveryScenario()
        {
            Database.SetInitializer(new DbSetup());
        }
    }


    [Binding]
    public class BrowsingProductsSteps
    {
        private HomeController _homeController;
        private ActionResult _result;
        private IUnityContainer _container;

        [BeforeScenario("HomePage")]
        public void InitScenario()
        {
            _container = Bootstrapper.Initialise();
            _homeController = _container.Resolve<HomeController>();
        }

        [When]
        public void When_I_navigate_to_Home_page()
        {
            _result = _homeController.Index();
        }
        
        [Then]
        public void Then_the_title_of_the_page_TITLE_is_displayed(string title)
        {
            ViewResult viewResult = (ViewResult) _result;
            Assert.That(viewResult.ViewBag.Title.ToString(), Is.EqualTo(title));
        }
        
        [Then]
        public void Then_the_list_of_products_in_alphabetical_order_is_displayed(Table table)
        {
            IEnumerable<Product> expectedProducts = table.CreateSet<Product>();
            
            ViewResult viewResult = (ViewResult) _result;

            var actualProducts = (IEnumerable<Product>)viewResult.Model;

            actualProducts
                .Should().Equal(expectedProducts, (p1, p2) => p1.Name == p2.Name);
        }
    }
}
