﻿using System;
using System.Linq;
using TDD.Model;
using TDD.Services.RequestResponse;

namespace TDD.Services
{
    public interface IProductService
    {
        GetAllProductsResponse GetAllProducts();
        AddNewProductResponse AddNewProduct(Product product);
    }

    public class ProductService : IProductService
    {
        private IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public GetAllProductsResponse GetAllProducts()
        {
            GetAllProductsResponse response = new GetAllProductsResponse();

            try
            {
                response.Success = true;
                var allProducts = _productRepository.GetAll();
                response.Products = allProducts.OrderBy(p => p.Name);
            }
            catch (Exception e)
            {
                response.Success = false;
                response.Message = "Sorry! Service doesn't work";
            }
            
            return response;
        }

        public AddNewProductResponse AddNewProduct(Product product)
        {
            var response = new AddNewProductResponse();

            if (_productRepository.FindByName(product.Name) == null)
            {
                _productRepository.Add(product);
                _productRepository.SaveChanges();
                response.Success = true;
                return response;
            }

            response.Success = false;
            response.Message = "Error! Product with the same name already exists.";

            return response;
        }
    }
}
