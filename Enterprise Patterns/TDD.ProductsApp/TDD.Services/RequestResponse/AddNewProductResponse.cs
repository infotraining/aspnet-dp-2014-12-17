﻿namespace TDD.Services.RequestResponse
{
    public class ResponseBase
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }

    public class AddNewProductResponse : ResponseBase
    {
    }
}