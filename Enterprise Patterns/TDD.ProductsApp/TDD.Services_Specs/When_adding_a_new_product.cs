using Machine.Specifications;
using Moq;
using TDD.Model;
using TDD.Services;
using TDD.Services.RequestResponse;
using TDD.Services_Tests;
using It = Machine.Specifications.It;

namespace TDD.Services_Specs
{
    [Tags("Browsing products")]
    [Tags("ProductService,HappyPath")]
    [Subject(typeof(ProductService))]
    public class When_adding_a_new_product : With_Service
    {
        Because of = () => { _addNewProductResponse = _productService.AddNewProduct(_product); };

        private It should_add_product_to_repository = () =>
        {
            _mqRepository.Verify(m => m.Add(_product), Times.Once);
            _mqRepository.Verify(m => m.SaveChanges(), Times.Once);
        };

        private It should_return_succesfull_response = () => _addNewProductResponse.Success.ShouldBeTrue();

        private It should_return_response = () => _addNewProductResponse.ShouldNotBeNull();

        private Behaves_like<ReturningResponse> returning_response;
    }
}