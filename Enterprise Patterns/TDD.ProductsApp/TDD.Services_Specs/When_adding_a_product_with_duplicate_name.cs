﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;
using Machine.Specifications;
using TDD.Model;
using TDD.Services;
using TDD.Services.RequestResponse;
using TDD.Services_Tests;
using It = Machine.Specifications.It;

namespace TDD.Services_Specs
{
    [Behaviors]
    public class ReturningResponse
    {
        protected static AddNewProductResponse _addNewProductResponse;

        private It should_return_response = () => _addNewProductResponse.ShouldNotBeNull();
    }

    [Tags("Browsing products")]
    [Tags("ProductService,SadPath")]
    [Subject(typeof (ProductService))]
    public class When_adding_a_product_with_duplicate_name : With_Service
    {
        Establish context = () =>
        {
            _mqRepository.Setup(m => m.FindByName(_product.Name)).Returns(_product);
        };

        Because of = () => _addNewProductResponse = _productService.AddNewProduct(_product); 

        private It should_not_add_product_to_repository = ()
            => _mqRepository.Verify(m => m.Add(Moq.It.IsAny<Product>()), Moq.Times.Never);

        //private It should_return_response_with_success_set_to_false =
        //    () => _addNewProductResponse.Success.ShouldBeFalse();

        private It should_return_response_with_error_message = () =>
        {
            _addNewProductResponse.Success.ShouldBeFalse();
            _addNewProductResponse.Message
                .ShouldEqual("Error! Product with the same name already exists.");
        };

        private Behaves_like<ReturningResponse> returning_response;
    }


}
