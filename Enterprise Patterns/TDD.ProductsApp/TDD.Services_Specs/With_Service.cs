﻿using Machine.Specifications;
using Moq;
using TDD.Model;
using TDD.Services;
using TDD.Services.RequestResponse;
using TDD.Services_Tests;

namespace TDD.Services_Specs
{
    public class With_Service
    {
        protected static ProductService _productService;
        protected static Mock<IProductRepository> _mqRepository;
        protected static Product _product = new Product() { Name = "P1" };
        protected static AddNewProductResponse _addNewProductResponse;

        Establish context = () =>
        {
            _mqRepository = new Mock<IProductRepository>();
            _productService = new ProductService(_mqRepository.Object);
        };
    }
}