using Moq;
using NUnit.Framework;
using TDD.Model;
using TDD.Services;
using TDD.Services.RequestResponse;

namespace TDD.Services_Tests
{
    //[TestFixture]
    public class With_ProductService
    {
        protected ProductService _productService;
        protected Mock<IProductRepository> _mqProductRepository;
        protected static Product _product = new Product() { Name = "P1" };
        protected static AddNewProductResponse _addNewProductResponse;

        [SetUp]
        public void CreateSut()
        {
            _mqProductRepository = new Mock<IProductRepository>();
            _productService = new ProductService(_mqProductRepository.Object);
        }
    }
}