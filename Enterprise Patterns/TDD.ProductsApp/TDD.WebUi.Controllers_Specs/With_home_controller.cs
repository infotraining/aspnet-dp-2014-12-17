﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Machine.Specifications;
using Machine.Specifications.Model;
using Machine.Specifications.Mvc;
using Moq;
using TDD.Model;
using TDD.Services;
using TDD.Services.RequestResponse;
using TDD.WebUi.Mvc.Controllers;
using It = Machine.Specifications.It;
using Arg = Moq.It;

namespace TDD.WebUi.Controllers_Specs
{
    public class With_home_controller
    {
        protected static HomeController _controller;
        protected static ActionResult _result;
        protected static Mock<IProductService> _mqProductService;

        Establish context = () =>
        {
            _mqProductService = new Mock<IProductService>();
            _controller = new HomeController(_mqProductService.Object);
        };
    }

    [Tags("Browsing products")]
    [Subject(typeof (HomeController))]
    public class When_browsing_products : With_home_controller
    {
        private static List<Product> _expectedProducts;

        Establish context = () =>
        {
            _expectedProducts = new List<Product>()
            {
                new Product() { Id = 1, Name = "P1"},
                new Product() { Id = 2, Name = "P2"},
                new Product() { Id = 3, Name = "P3"},
            };

            _mqProductService.Setup(m => m.GetAllProducts())
                .Returns(new GetAllProductsResponse() {Success = true, Products = _expectedProducts});
        };

        Because of = () => _result = _controller.Index();

        private It should_return_ViewResult = () => _result.ShouldBeAView();

        private It should_call_service_for_products = () =>
        {
            _mqProductService.Verify(m => m.GetAllProducts());
        };

        private It should_return_list_of_products = () =>
        {
            _result.Model<IEnumerable<Product>>().ShouldBeTheSameAs(_expectedProducts);
        };
    }
}
