﻿using System.Collections.Generic;
namespace DependencyInjection.Model
{
    public interface IProductRepository
    {
        IEnumerable<Product> FindAll();
    }
}
