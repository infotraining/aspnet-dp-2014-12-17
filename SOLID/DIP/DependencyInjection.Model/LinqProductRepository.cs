﻿using System.Collections.Generic;

namespace DependencyInjection.Model
{
    public class LinqProductRepository : IProductRepository
    {
        public IEnumerable<Product> FindAll()
        {            
            return new List<Product>();
        }     
    }
}
