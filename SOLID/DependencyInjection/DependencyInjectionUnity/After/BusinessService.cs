﻿using DependencyInjectionUnity.Dependencies;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyInjectionUnity.After
{
    public class BusinessService
    {
        private IDataAccessComponent _dataAccessComponent;
        private ILoggingComponent _loggingComponent;
        private IWebServiceProxy _webServiceProxy;

        public BusinessService(IDataAccessComponent dataAccessComponent, IWebServiceProxy webServiceProxy, ILoggingComponent loggingComponent)
        {
            _dataAccessComponent = dataAccessComponent;
            _webServiceProxy = webServiceProxy;
            _loggingComponent = loggingComponent;
        }

        public decimal GetPriceById(int id)
        {
            _loggingComponent.LogMessage(string.Format("GetPriceById({0}) : {1}", id, DateTime.Now));
            string symbol = _dataAccessComponent.GetSymbol(id);
            decimal price = _webServiceProxy.GetPrice(symbol);
            return price;
        }
    }
}
