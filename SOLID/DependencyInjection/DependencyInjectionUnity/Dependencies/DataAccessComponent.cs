﻿using System;

namespace DependencyInjectionUnity.Dependencies
{
    public interface IDataAccessComponent
    {
        string GetSymbol(int id);
    }

    public class DataAccessComponent : IDataAccessComponent
    {
        private string _connectionString;

        public DataAccessComponent(string connectionString)
        {
            this._connectionString = connectionString;
        }

        public string GetSymbol(int id)
        {
            Console.WriteLine("Connecting to DB: {0}", _connectionString);

            return "IBM";
        }
    }
}
