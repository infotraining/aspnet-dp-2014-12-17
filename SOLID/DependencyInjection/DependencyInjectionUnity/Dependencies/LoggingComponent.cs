﻿namespace DependencyInjectionUnity.Dependencies
{
    public interface ILoggingComponent
    {
        void LogMessage(string message);
    }

    public class LoggingComponent : ILoggingComponent
    {
        private string _logName;
        private ILoggingDataSink _loggingDataSink;

        public LoggingComponent(ILoggingDataSink loggingDataSink, string logName)
        {
            _loggingDataSink = loggingDataSink;
            _logName = logName;
        }

        public void LogMessage(string message)
        {
            _loggingDataSink.Write(_logName + " >>> " + message);
        }
    }
}
