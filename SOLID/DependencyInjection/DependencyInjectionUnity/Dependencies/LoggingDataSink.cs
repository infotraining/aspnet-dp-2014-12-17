﻿using System;

namespace DependencyInjectionUnity.Dependencies
{
    public interface ILoggingDataSink
    {
        void Write(string message);
    }
    public class ConsoleLoggingDataSink : ILoggingDataSink
    {
        public void Write(string message)
        {
            Console.WriteLine("Logging in console: {0}", message);
        }
    }

    public class DbLoggingDataSink : ILoggingDataSink
    {
        private readonly string _connectionString;

        public DbLoggingDataSink(string connectionString)
        {
            _connectionString = connectionString;
        }

        public void Write(string message)
        {
            Console.WriteLine("Logging in Db[{0}: {1}", _connectionString, message);
        }
    }
}
