﻿using System;

namespace DependencyInjectionUnity.Dependencies
{
    public interface IWebServiceProxy
    {
        decimal GetPrice(string symbol);
    }

    public class WebServiceProxy : IWebServiceProxy
    {
        private string _address;

        public WebServiceProxy(string address)
        {
            _address = address;
        }

        public decimal GetPrice(string symbol)
        {
            Console.WriteLine("Connecting to web-service: {0}", _address);

            return 10.0M;
        }
    }
}
