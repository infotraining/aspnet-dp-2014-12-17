﻿using System.Configuration;
using DependencyInjectionUnity.Dependencies;

namespace DependencyInjectionUnity.Factories
{


    public interface IDataAccessComponentFactory
    {
        IDataAccessComponent Create(object context);
    }

    public interface IWebServiceProxyFactory
    {
        IWebServiceProxy Create(object context);
    }

    public class DataAccessComponentFactory : IDataAccessComponentFactory
    {
        public IDataAccessComponent Create(object context)
        {
            var databaseConnectionString
                = ConfigurationManager.ConnectionStrings["MyConnectionString"].ConnectionString;

            return new DataAccessComponent(databaseConnectionString);
        }
    }

    public class WebServiceProxyFactory : IWebServiceProxyFactory
    {
        public IWebServiceProxy Create(object context)
        {
            string webServiceAddress = ConfigurationManager.AppSettings["MyWebServiceAddress"];

            return new WebServiceProxy(webServiceAddress);
        }
    }

}
