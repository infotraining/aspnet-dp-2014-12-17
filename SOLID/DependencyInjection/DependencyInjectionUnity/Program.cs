﻿using DependencyInjectionUnity.After;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DependencyInjectionUnity.Dependencies;

namespace DependencyInjectionUnity
{

    public class ContainerBootstrapper
    {
        public static void RegisterTypes(IUnityContainer container)
        {
            string webServiceAddress = ConfigurationManager.AppSettings["MyWebServiceAddress"];

            container
                .RegisterType<ILoggingDataSink, DbLoggingDataSink>(new InjectionConstructor(typeof(string)))
                .RegisterType<ILoggingComponent, LoggingComponent>(
                    new InjectionConstructor(typeof(ILoggingDataSink), typeof(string)))
                .RegisterType<IDataAccessComponent, DataAccessComponent>(new ContainerControlledLifetimeManager(), 
                    new InjectionConstructor(typeof(string)))
                .RegisterType<IWebServiceProxy, WebServiceProxy>(new InjectionConstructor(webServiceAddress));
        }
    }

    internal class Program
    {
        private static void Main(string[] args)
        {
            //BusinessService service = new BusinessService();

            IUnityContainer ioc = new UnityContainer();

            ContainerBootstrapper.RegisterTypes(ioc);

            foreach (var r in ioc.Registrations)
            {
                Console.WriteLine(r.GetMappingAsString());
            }

            Console.WriteLine();

            After.BusinessService service =
                ioc.Resolve<After.BusinessService>(
                    new ParameterOverride("connectionString", "LogDB").OnType<DbLoggingDataSink>(),
                    new ParameterOverride("logName", "AppLog"),
                    new ParameterOverride("connectionString", "AzureDB").OnType<DataAccessComponent>());

            decimal price = service.GetPriceById(10);

            Console.WriteLine("Price {0:c}", price);

            Console.WriteLine();

            UnityContainer localIoC = new UnityContainer();

            localIoC.RegisterType<ILoggingDataSink, ConsoleLoggingDataSink>("ConsoleLog");
            localIoC.RegisterType<ILoggingDataSink, DbLoggingDataSink>("DbLog", new InjectionConstructor(typeof(string)));

            foreach (var r in localIoC.Registrations)
            {
                Console.WriteLine(r.GetMappingAsString());
            }
            Console.WriteLine();

            var log = localIoC.Resolve<ILoggingDataSink>("ConsoleLog", new ParameterOverride("connectionString", "DB_LOG"));

            log.Write("Test loggera");
        }
    }

    internal static class ContainerRegistrationsExtension
    {
        public static string GetMappingAsString(
            this ContainerRegistration registration)
        {
            string regName, regType, mapTo, lifetime;
            var r = registration.RegisteredType;
            regType = r.Name + GetGenericArgumentsList(r);
            var m = registration.MappedToType;
            mapTo = m.Name + GetGenericArgumentsList(m);
            regName = registration.Name ?? "[default]";
            lifetime = registration.LifetimeManagerType.Name;
            if (mapTo != regType)
            {
                mapTo = " -> " + mapTo;
            }
            else
            {
                mapTo = string.Empty;
            }
            lifetime = lifetime.Substring(
                0, lifetime.Length - "LifetimeManager".Length);
            return string.Format(
                "+ {0}{1} '{2}' {3}", regType, mapTo, regName, lifetime);
        }

        private static string GetGenericArgumentsList(Type type)
        {
            if (type.GetGenericArguments().Length == 0) return string.Empty;
            string arglist = string.Empty;
            bool first = true;

            foreach (Type t in type.GetGenericArguments())
            {
                arglist += first ? t.Name : ", " + t.Name;
                first = false;
                if (t.GetGenericArguments().Length > 0)
                {
                    arglist += GetGenericArgumentsList(t);
                }
            }
            return "<" + arglist + ">";
        }
    }
}