﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiskovSubstitutionPrinciple.Mocks;

namespace LiskovSubstitutionPrinciple
{
    public class PayPalPayment : PaymentServiceBase
    {
        public string AccountName { get; set; }
        public string Password { get; set; }

        public PayPalPayment(string accountName, string password)
        {
            AccountName = accountName;
            Password = password;
        }

        public override RefundResponse Refund(decimal amount, string transactionId)
        {

            MockPayPalWebService payPalWebService = new MockPayPalWebService();

            string token = payPalWebService.ObtainToken(AccountName, Password);

            var response = new RefundResponse();

            response.Message = payPalWebService.MakeRefund(amount, transactionId, token);

            response.Success = response.Message.Contains("A_Success");

            return response;
        }
    }
}
