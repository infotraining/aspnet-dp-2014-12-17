﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiskovSubstitutionPrinciple
{
    public enum PaymentType
    {
        PayPal = 1,
        WorldPay = 2
    }
}
