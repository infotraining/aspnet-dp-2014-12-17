﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiskovSubstitutionPrinciple.Mocks;

namespace LiskovSubstitutionPrinciple
{
    public class WorldPayPayment : PaymentServiceBase
    {
        public string AccountId { get; set; }
        public string AccountPassword { get; set; }
        public string ProductId { get; set; }

        public WorldPayPayment(string accountId, string accountPassword, string productId)
        {
            AccountId = accountId;
            AccountPassword = accountPassword;
            ProductId = productId;
        }

        public override RefundResponse Refund(decimal amount, string transactionId)
        {
            MockWorldPayWebService worldPayWebService = new MockWorldPayWebService();

            var response = new RefundResponse();

            response.Message = worldPayWebService.MakeRefund(amount, transactionId, AccountId, AccountPassword, ProductId);
            response.Success = response.Message.Contains("Auth");

            return response;
        }
    }
}
