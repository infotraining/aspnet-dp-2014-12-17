﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Data.Common;
using System.Dynamic;
using System.Linq;

namespace Core.StoreExample
{

    public interface IDiscountRule
    {
        decimal CalculateCustomerDiscount(Customer customer);
    }


    [Export(typeof(IDiscountRule))]
    public class FirstTimeCustomerRule : IDiscountRule
    {
        public decimal CalculateCustomerDiscount(Customer customer)
        {
            if (!customer.DateOfFirstPurchase.HasValue)
            {
                return 0.15m;
            }
            return 0;
        }
    }

    public class LoyalCustomerRule : IDiscountRule
    {
        private readonly decimal _discount;
        private readonly int _yearsAsCustomer;

        public LoyalCustomerRule(int yearsAsCustomer, decimal discount)
        {
            this._yearsAsCustomer = yearsAsCustomer;
            this._discount = discount;
        }

        public decimal CalculateCustomerDiscount(Customer customer)
        {
            if (customer.DateOfFirstPurchase.HasValue)
            {
                if (customer.DateOfFirstPurchase.Value.AddYears(_yearsAsCustomer) <= DateTime.Today)
                {
                    var birthdayRule = new BirthdayDiscountRule();

                    return _discount + birthdayRule.CalculateCustomerDiscount(customer);
                }
            }
            return 0;
        }
    }


    [Export(typeof(IDiscountRule))]
    public class VeteranRule : IDiscountRule
    {
        public decimal CalculateCustomerDiscount(Customer customer)
        {
            if (customer.IsVeteran)
            {
                return 0.1m;
            }
            return 0;
        }
    }

    [Export(typeof(IDiscountRule))]
    public class SeniorRule : IDiscountRule
    {
        public decimal CalculateCustomerDiscount(Customer customer)
        {
            if (customer.DateOfBirth < DateTime.Now.AddYears(-65))
            {
                return .05m;
            }
            return 0;
        }
    }

    [Export(typeof(IDiscountRule))]
    public class BirthdayDiscountRule : IDiscountRule
    {
        public decimal CalculateCustomerDiscount(Customer customer)
        {
            if (customer.DateOfBirth.Month == DateTime.Today.Month &&
                customer.DateOfBirth.Day == DateTime.Today.Day)
            {
                return 0.10m;
            }
            return 0;
        }
    }

    [Export]
    public class DiscountCalculator
    {
        [ImportMany]
        public IList<IDiscountRule> Rules { get; set; }
        
        private DiscountCalculator()
        {
            Rules = new List<IDiscountRule>();            
        }

        public decimal CalculateDiscountPercentage(Customer customer)
        {
            decimal discount = 0;
            foreach (var rule in Rules)
            {
                discount = Math.Max(rule.CalculateCustomerDiscount(customer), discount);
            }
            return discount;
        }

        public static DiscountCalculator Create()
        {
            AssemblyCatalog catalog = new AssemblyCatalog(typeof(IDiscountRule).Assembly);

            CompositionContainer container = new CompositionContainer(catalog);

            DiscountCalculator calculator = container.GetExportedValue<DiscountCalculator>();

            calculator.Rules.Add(new LoyalCustomerRule(1, 0.10m));
            calculator.Rules.Add(new LoyalCustomerRule(5, 0.12m));
            calculator.Rules.Add(new LoyalCustomerRule(10, 0.20m));

            return calculator;
        }
    }
}